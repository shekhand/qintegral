import numpy as np
from itertools import product
from functools import lru_cache

from joblib import Parallel, delayed

# https://en.wikipedia.org/wiki/M%C3%B6ller%E2%80%93Trumbore_intersection_algorithm
# https://www.scratchapixel.com/lessons/3d-basic-rendering/ray-tracing-rendering-a-triangle/moller-trumbore-ray-triangle-intersection
def ray_intersection(orig, dir, verts, eps=2**-8):
    v0, v1, v2 = verts
    e1 = v1 - v0
    e2 = v2 - v0
    pvec = np.cross(dir, e2)
    det = e1.dot(pvec)

    if det < eps and det > -eps:
        return 0

    tvec = orig - v0
    u = tvec.dot(pvec) / det
    if u < 0 or u > 1:
        return 0

    qvec = np.cross(tvec, e1)
    v = dir.dot(qvec) / det
    if v < 0 or (u + v) > 1:
        return 0

    t = e2.dot(qvec) / det
    return t


def make_point(a, b, rds, vectors):
    P = (b - a) * np.random.rand(3) + a
    res = []
    for rd in rds:
        ip, im = False, False
        for v in vectors:
            t = ray_intersection(P, rd, v)

            if t > 0:
                ip = True
            elif t < 0:
                im = True
            if ip and im:
                break

        res.append(ip and im)
    return (*P, all(res))


def montecarlo_parallel(mesh, xlim, ylim, zlim, n, n_jobs=4):
    rds = np.eye(3)

    a, b = zip(xlim, ylim, zlim)
    a, b = np.array(a), np.array(b)

    points = Parallel(n_jobs=n_jobs, verbose=10)(delayed(make_point)(a, b, rds, mesh.vectors) for i in range(n))

    x, y, z, inside = zip(*points)
    k = inside.count(True)
    vol = (k / n) * np.prod(b - a)
    return vol, (x, y, z, inside)


def make_rectangle(x1, y1, zlim, vectors):
    rd = np.array([0, 0, 1])
    P = np.array([x1, y1, zlim])

    ts = (ray_intersection(P, rd, v) for v in vectors)

    return max(ts)


def rectanlge_method_parallel(mesh, xlim, ylim, zlim, n, n_jobs=4):
    x = np.linspace(*xlim, n+1)
    y = np.linspace(*ylim, n+1)

    x_means = (x[:-1] + x[1:])/2
    y_means = (y[:-1] + y[1:])/2
    z = Parallel(n_jobs=n_jobs, verbose=10)(delayed(make_rectangle)(x1, y1, zlim[0], mesh.vectors)
                                            for y1, x1 in product(y_means, x_means))
    z = np.array(z)

    x_width = (xlim[1] - xlim[0])/n
    y_width = (ylim[1] - ylim[0])/n
    vol = np.sum(z * x_width * y_width)
    x, y = np.meshgrid(x[:-1], y[:-1])
    x, y = x.ravel(), y.ravel()
    return vol, (x, y, z)
