import numpy as np
from mpl_toolkits import mplot3d

def show_model(ax, model, lims):
    ax.add_collection3d(mplot3d.art3d.Poly3DCollection(model.vectors, edgecolor='k', alpha=0.05))

    ax.auto_scale_xyz(*lims)

    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')


def rectanlge_visual(ax, model, x, y, z, n, lims):
    ax.add_collection3d(mplot3d.art3d.Poly3DCollection(model.vectors, edgecolor='k', alpha=0.05))

    w = [(s[1] - s[0])/n for s in lims]

    bottom = lims[2][0] * np.ones(np.prod(x.shape))
    ax.bar3d(x, y, bottom, w[0], w[1], z, shade=True,
             alpha=0.3) 

    ax.auto_scale_xyz(*lims)

    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')


def montecarlo_visual(ax, model, x, y, z, inside, n, lims):
    ax.add_collection3d(mplot3d.art3d.Poly3DCollection(model.vectors, edgecolor='k', alpha=0.05))

    ax.scatter3D(x, y, z, c=['g' if i else 'r' for i in inside], alpha=0.7, marker='+')

    ax.auto_scale_xyz(*lims)

    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')
