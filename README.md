Application for computing the volume of models in STL format. Written in PyQt5 and Python.

# Installation:

Application was written and tested on Python 3.9. It could not run correctly on the lower version.

```bash
git clone https://gitlab.com/shekhand/qintegral
cd qintegral
pip install -r requirments.txt
python main.py
```

Main window of the application:
![alt text](images/screenshot.png?raw=true)
