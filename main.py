import sys
import os
import os.path
import time

from stl import mesh
import numpy as np

from joblib import cpu_count

from PyQt5 import QtCore, QtGui
from PyQt5.QtWidgets import *

from matplotlib.backends.backend_qt5agg import (
    FigureCanvas, NavigationToolbar2QT as NavigationToolbar)
from matplotlib.figure import Figure
from mpl_toolkits import mplot3d
from matplotlib import pyplot as plt

from algorithms import montecarlo_parallel, rectanlge_method_parallel
from visual import montecarlo_visual, rectanlge_visual, show_model

__version__ = '0.0.3'

class ApplicationWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.main = QWidget()
        self.setCentralWidget(self.main)

        print(f'Katalog roboczy to "{os.getcwd()}"\n')
        self.current_mesh = None
        self.last_call_sign = None
        self.last_call_result = None

        layout = QHBoxLayout(self.main)

        self.figure = Figure(figsize=(7, 7))
        self.ax = mplot3d.Axes3D(self.figure)

        self.canvas = FigureCanvas(self.figure)
        layout.addWidget(self.canvas)

        side_layout = QVBoxLayout(self.main)
        layout.addLayout(side_layout)

        qintegral = QLabel(f'QIntegral {__version__}')
        qintegral.setFont(QtGui.QFont('Monospace', 16, QtGui.QFont.TypeWriter))
        qintegral.setAlignment(QtCore.Qt.AlignCenter)
        side_layout.addWidget(qintegral)

        side_layout.addWidget(QLabel('Wybierz plik .stl:'))

        file_choose_layout = QHBoxLayout()
        side_layout.addLayout(file_choose_layout)

        self.file_choose_edit = QLineEdit()
        self.file_choose_edit.setReadOnly(True)
        self.file_choose_edit.textChanged.connect(self.new_file_chosen_slot)
        file_choose_layout.addWidget(self.file_choose_edit)

        self.file_choose_button = QPushButton('Wybierz plik')
        self.file_choose_button.clicked.connect(self.choose_file_slot)
        file_choose_layout.addWidget(self.file_choose_button)

        side_layout.addWidget(QLabel('Wybierz algorytm:'))
        self.algorithm_combobox = QComboBox()
        self.algorithm_combobox.addItems(['Metoda prostokątów', 'Monte Carlo'])
        self.algorithm_combobox.currentIndexChanged.connect(self.algorithm_changed_slot)
        side_layout.addWidget(self.algorithm_combobox)

        self.number_n_label = QLabel('Liczba prostokątów')
        side_layout.addWidget(self.number_n_label)
        self.number_n_spin = QSpinBox()
        self.number_n_spin.setRange(1, 100000)
        self.number_n_spin.setValue(10)
        side_layout.addWidget(self.number_n_spin)

        side_layout.addWidget(QLabel('Liczba wątków do obliczeń'))
        self.n_jobs_spin = QSpinBox()
        cc = cpu_count()
        self.n_jobs_spin.setRange(1, cc)
        self.n_jobs_spin.setValue(cc // 2)
        side_layout.addWidget(self.n_jobs_spin)

        self.manual_lims_checkbox = QCheckBox('Ręczne granice')
        self.manual_lims_checkbox.setCheckState(0)
        self.manual_lims_checkbox.stateChanged.connect(self.manual_lims_slot)
        side_layout.addWidget(self.manual_lims_checkbox)

        self.lims_spins = []
        for ax in ['X', 'Y', 'Z']:
            side_layout.addWidget(QLabel(f'Granice osi {ax}:'))
            tmp_layout = QHBoxLayout()
            side_layout.addLayout(tmp_layout)

            tmp_layout.addWidget(QLabel('Min: '))
            l1 = QDoubleSpinBox()
            l1.setReadOnly(True)
            l1.setRange(-1000, 1000)
            tmp_layout.addWidget(l1)

            tmp_layout.addStretch(1)

            tmp_layout.addWidget(QLabel('Max: '))
            l2 = QDoubleSpinBox()
            l2.setReadOnly(True)
            l2.setRange(-1000, 1000)
            tmp_layout.addWidget(l2)

            self.lims_spins.append([l1, l2])

        self.savefig_checkbox = QCheckBox('Zapisz wykres do pliku')
        self.savefig_checkbox.setCheckState(0)
        side_layout.addWidget(self.savefig_checkbox)

        self.new_window_checkbox = QCheckBox('Pokaż wykres w nowym oknie')
        self.new_window_checkbox.setCheckState(0)
        side_layout.addWidget(self.new_window_checkbox)

        self.force_calc_checkbox = QCheckBox('Wymuś powtórne obliczenia')
        self.force_calc_checkbox.setCheckState(0)
        side_layout.addWidget(self.force_calc_checkbox)

        self.calculate_button = QPushButton('Oblicz')
        self.calculate_button.clicked.connect(self.calculate_slot)
        side_layout.addWidget(self.calculate_button)

        self.result_label = QLabel('')
        self.result_label.setFont(QtGui.QFont('Monospace', 14, QtGui.QFont.TypeWriter))
        side_layout.addWidget(self.result_label)

        side_layout.addStretch(1)

        self.exit_button = QPushButton('Zakończ program')
        self.exit_button.clicked.connect(self.exit_slot)
        side_layout.addWidget(self.exit_button)


    def new_file_chosen_slot(self, file):
        self.current_mesh = mesh.Mesh.from_file(file)
        lims = self.get_lims_from_mesh()

        for (l1, l2), (v1, v2) in zip(self.lims_spins, lims):
            l1.setValue(v1)
            l2.setValue(v2)

        self.ax.clear()
        show_model(self.ax, self.current_mesh,
                   lims)
        self.figure.canvas.draw()

    def get_lims_from_mesh(self):
        if self.current_mesh is None:
            return [[0, 0]] * 3
        lims = [[np.min(self.current_mesh.vectors[:, :, i]),
                 np.max(self.current_mesh.vectors[:, :, i])]
                 for i in range(3)]
        max_range = max(l2 - l1 for l1, l2 in lims)

        fixed_lims = []
        for l1, l2 in lims:
            pad = (max_range - (l2 - l1)) / 2
            fixed_lims.append([l1 - pad, l2 + pad])

        return fixed_lims

    def choose_file_slot(self):
        file, _ = QFileDialog.getOpenFileName(self,
                'Wybierz plik STL',
                '.',
                '*.stl')
        if file:
            self.file_choose_edit.setText(file)

    def calculate_slot(self):
        if self.current_mesh is None:
            self.show_error('Najpierw trzeba wybrać plik!')
            return

        lims = [(l1.value(), l2.value()) for l1, l2 in self.lims_spins]
        n = self.number_n_spin.value()
        n_jobs = self.n_jobs_spin.value()

        met = self.algorithm_combobox.currentIndex()
        # Zapytać kiedy dajemy za duże n
        if met == 0 and n > 50 and not self.continue_question():
            return
        if met == 1 and n > 10000 and not self.continue_question():
            return

        if met == 0:
            method, method_vis = rectanlge_method_parallel, rectanlge_visual
        elif met == 1:
            method, method_vis = montecarlo_parallel, montecarlo_visual

        call_sign = hash(''.join(str(i) for i in [method, self.current_mesh, lims, n]))
        force_calc = self.force_calc_checkbox.isChecked()
        start = time.time()
        if not force_calc and call_sign == self.last_call_sign:
            print('\n\nZaładowany ostatni wynik.')
            vol, vis = self.last_call_result
        else:
            print('\n\nTrwają obliczenia...')
            vol, vis = method(self.current_mesh, *lims, n, n_jobs)

            self.last_call_sign = call_sign
            self.last_call_result = (vol, vis)
        end = time.time()

        self.result_label.setText(
                f'Czas: {end - start:>14.4f}s\nObjętość: {vol:>10.4f}')

        self.ax.clear()
        method_vis(self.ax, self.current_mesh, *vis, n, lims)
        #  self.figure.suptitle(f'Obiętość: {vol: 0.4f}')
        self.figure.canvas.draw()

        if self.savefig_checkbox.isChecked():
            self.savefig()

        if self.new_window_checkbox.isChecked():
            fig = plt.figure(figsize=(8, 8))
            ax = mplot3d.Axes3D(fig)
            method_vis(ax, self.current_mesh, *vis, n, lims)
            #  fig.suptitle(f'Obiętość: {vol: 0.4f}')
            plt.show()

    def savefig(self):
        cwd = os.getcwd()
        images = os.path.join(cwd, 'images')
        if not os.path.isdir(images):
            print(f'\nKatalog "{images}" został utworzony.')
            os.mkdir(images)
        else:
            print(f'\nKatalog "{images}" już istnieje.')

        filename = self.file_choose_edit.text()
        model_name = os.path.split(filename)[1].split('.')[0]

        m = self.algorithm_combobox.currentIndex()
        m = 'rect' if m == 0 else 'monte'
        n = self.number_n_spin.value()
        filename = os.path.join(images, f'{model_name}_{m}_{n}.png')
        self.figure.savefig(filename, bbox_inches='tight')
        print(f'\nPlik został zapisany jako "{filename}"')

    def manual_lims_slot(self, val):
        # val == 0 - unchecked
        # val == 2 - checked
        for l1, l2 in self.lims_spins:
            l1.setReadOnly(val == 0)
            l2.setReadOnly(val == 0)

        if val == 0:
            lims = self.get_lims_from_mesh()
            for (l1, l2), (v1, v2) in zip(self.lims_spins, lims):
                l1.setValue(v1)
                l2.setValue(v2)

    def algorithm_changed_slot(self, val):
        if val == 0:
            self.number_n_spin.setValue(10)
            self.number_n_label.setText('Liczba prostokątów')
        elif val == 1:
            self.number_n_spin.setValue(100)
            self.number_n_label.setText('Liczba punktów')

    def show_error(self, text, title='Error', icon=QMessageBox.Critical):
        msg = QMessageBox()
        msg.setIcon(icon)
        msg.setText(title)
        msg.setInformativeText(text)
        msg.setWindowTitle(title)
        msg.exec_()

    def continue_question(self):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Warning)
        msg.setText('Uwaga!')
        msg.setInformativeText('Obliczenia dla tak dużej liczby n mogą trwać długi czas. Na pewno chcesz kontynuować?')
        msg.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
        msg.setWindowTitle('Uwaga!')
        ret = msg.exec_()

        if ret == QMessageBox.Ok:
            return True
        else:
            return False

    def exit_slot(self):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Warning)
        msg.setText('Uwaga!')
        msg.setInformativeText('Na pewno chcesz wyjść z programu?')
        msg.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
        msg.setWindowTitle('Zakończenie programu')
        ret = msg.exec_()

        if ret == QMessageBox.Ok:
            self.close()
        else:
            return


if __name__ == "__main__":
    qapp = QApplication.instance()
    if not qapp:
        qapp = QApplication(sys.argv)

    app = ApplicationWindow()
    app.setWindowTitle('QIntegral')
    app.show()
    app.activateWindow()
    app.raise_()
    qapp.exec_()
